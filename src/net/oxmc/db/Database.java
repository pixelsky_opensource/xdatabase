package net.oxmc.db;

import org.apache.commons.dbcp.BasicDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * API для других плагинов
 * Содержит метод getConnection(), позволяющий получить свободное соединение с БД
 */
public class Database {
    private static Database instance;

    // Источник соединений
    private BasicDataSource dataSource;
    private Logger logger;

    static Database getInstance() {
        if (instance == null) {
            synchronized (Database.class) {
                if (instance == null) {
                    instance = new Database();
                }
            }
        }
        return instance;
    }

    /**
     * Вызывается один раз в onEnable плагина
     */
    static void setDataSource(BasicDataSource source) {
        getInstance().dataSource = source;
    }
    /**
     * Вызывается один раз в onEnable плагина
     */
    static void setLogger(Logger logger) {
        getInstance().logger = logger;
    }

    /**
     * Единственный метод, который должны использовать другие плагины
     * Возвращает соединение с БД
     *
     * В случае, если параметры соединения не нстроены возвращает null
     * Примечание: если Вы хотите использовать этот метод в своем плагине, не забудьте поставить в plugin.yml
     *             строку depend: [xDatabase], в результате этого onEnable Вашего плагина вызовется позже onEnable xDatabase.
     *             и к тому времени соединение будет уже настроено
     */
    public static Connection getConnection() throws SQLException {
        DataSource dataSource = getInstance().dataSource;
        if (dataSource == null)
            return null;
        return dataSource.getConnection();
    }

    /**
     * true, если подключение установлено, false, если нет
     */
    static boolean testConnection() {
        Connection con = null;
        try {
            con = getConnection();
            if (con != null)
              return con.isValid(10);
            else
                return false;
        } catch (SQLException e) {
            getInstance().logger.log(Level.SEVERE, "Error checking connection", e);
        } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    getInstance().logger.log(Level.SEVERE, "Error closing pool connection", e);
                }
            }
        }
        return false;
    }

    /**
     * Закрывает соединение с БД
     */
    static void shutdown() {
        try {
            getInstance().dataSource.close();
        } catch (SQLException e) {
            getInstance().logger.log(Level.SEVERE, "Error closing connection pool", e);
        }
    }
}
